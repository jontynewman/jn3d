compiler = glslangValidator
flags = -V
prefix = jn3d/vulkan/shaders/
basename = shader
src = \
$(addprefix ${prefix}, \
	$(addprefix ${basename}, \
		.vert \
		.frag \
	) \
)
bin = \
$(addprefix ${prefix}, \
	$(addsuffix .spv, \
		$(subst .,,$(suffix $(src))) \
	) \
)
cglm = cglm/build/libcglm.a
glfw = glfw/build/src/libglfw3.a
stb = stb.o
tinyobjloader = tinyobjloader.o
libs = \
	${cglm} \
	${glfw} \
	${stb} \
	${tinyobjloader}

build: ${bin} ${libs}
	zig build

run: ${bin} ${libs}
	zig build run -- $(filter-out $@,$(MAKECMDGOALS))

${bin}:
	${compiler} ${flags} ${prefix}${basename}.$(notdir $(basename $@)) -o $@

${cglm}:
	cmake -S cglm -B cglm/build -DCGLM_SHARED=OFF -DCGLM_STATIC=ON
	make -C cglm/build

${glfw}:
	cmake -S glfw -B glfw/build
	make -C glfw/build

${stb}:
	gcc -c stb.c -Istb

${tinyobjloader}:
	gcc -c tinyobjloader.c -Itinyobjloader-c

clean: clean-bin clean-libs
	rm -rf zig-out zig-cache

clean-bin:
	rm -f ${bin}

clean-libs:
	rm -f ${libs}

%:
	@:
