const Image = @This();

const c = @import("root").c;

pixels: *u8,
width: usize,
height: usize,
channels: usize,

pub fn init(data: []const u8) !Image {
    var width: i32 = undefined;
    var height: i32 = undefined;
    var channels: i32 = undefined;

    const pixels = c.stbi_load_from_memory(
        @ptrCast(data),
        @intCast(data.len),
        &width,
        &height,
        &channels,
        c.STBI_rgb_alpha,
    );

    return Image{
        .pixels = pixels,
        .width = @intCast(width),
        .height = @intCast(height),
        .channels = @intCast(channels),
    };
}

pub fn deinit(image: *const Image) void {
    c.stbi_image_free(image.pixels);
}
