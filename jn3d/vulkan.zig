pub const Buffer = @import("vulkan/Buffer.zig");
pub const Frame = @import("vulkan/Frame.zig");
pub const Image = @import("vulkan/Image.zig");
pub const QueueFamily = @import("vulkan/QueueFamily.zig");
pub const Swapchain = @import("vulkan/Swapchain.zig");
pub const UniformBuffer = @import("vulkan/UniformBuffer.zig");
pub const Vulkan = @import("vulkan/Vulkan.zig");

pub const layers = if (.Debug == @import("builtin").mode) [_][:0]const u8{
    "VK_LAYER_KHRONOS_validation",
} else [_][:0]const u8{};

const c = @import("root").c;
const std = @import("std");
const Allocator = std.mem.Allocator;
const assert = std.debug.assert;

pub const VulkanError = error{
    OutOfHostMemory,
    OutOfDeviceMemory,
    InitializationFailed,
    DeviceLost,
    MemoryMapFailed,
    LayerNotPresent,
    ExtensionNotPresent,
    FeatureNotPresent,
    IncompatibleDriver,
    TooManyObjects,
    FormatNotSupported,
    FragmentedPool,
    Unknown,
    OutOfPoolMemory,
    InvalidExternalHandle,
    Fragmentation,
    InvalidOpaqueCaptureAddress,
    SurfaceLostKhr,
    NativeWindowInUseKhr,
    OutOfDateKhr,
    IncompatibleDisplayKhr,
    ValidationFailedExt,
    InvalidShaderNv,
    ImageUsageNotSupportedKhr,
    VideoPictureLayoutNotSupportedKhr,
    VideoProfileOperationNotSupportedKhr,
    VideoProfileFormatNotSupportedKhr,
    VideoProfileCodecNotSupportedKhr,
    VideoStdVersionNotSupportedKhr,
    InvalidDrmFormatModifierPlaneLayoutExt,
    NotPermittedKhr,
    FullScreenExclusiveModeLostExt,
    CompressionExhaustedExt,
};

pub fn err(value: c.VkResult) VulkanError!void {
    _ = try result(value);
}

pub fn result(value: c.VkResult) VulkanError!c.VkResult {
    return switch (value) {
        -1 => VulkanError.OutOfHostMemory,
        -2 => VulkanError.OutOfDeviceMemory,
        -3 => VulkanError.InitializationFailed,
        -4 => VulkanError.DeviceLost,
        -5 => VulkanError.MemoryMapFailed,
        -6 => VulkanError.LayerNotPresent,
        -7 => VulkanError.ExtensionNotPresent,
        -8 => VulkanError.FeatureNotPresent,
        -9 => VulkanError.IncompatibleDriver,
        -10 => VulkanError.TooManyObjects,
        -11 => VulkanError.FormatNotSupported,
        -12 => VulkanError.FragmentedPool,
        -13 => VulkanError.Unknown,
        -1000069000 => VulkanError.OutOfPoolMemory,
        -1000072003 => VulkanError.InvalidExternalHandle,
        -1000161000 => VulkanError.Fragmentation,
        -1000257000 => VulkanError.InvalidOpaqueCaptureAddress,
        -1000000000 => VulkanError.SurfaceLostKhr,
        -1000000001 => VulkanError.NativeWindowInUseKhr,
        -1000001004 => VulkanError.OutOfDateKhr,
        -1000003001 => VulkanError.IncompatibleDisplayKhr,
        -1000011001 => VulkanError.ValidationFailedExt,
        -1000012000 => VulkanError.InvalidShaderNv,
        -1000023000 => VulkanError.ImageUsageNotSupportedKhr,
        -1000023001 => VulkanError.VideoPictureLayoutNotSupportedKhr,
        -1000023002 => VulkanError.VideoProfileOperationNotSupportedKhr,
        -1000023003 => VulkanError.VideoProfileFormatNotSupportedKhr,
        -1000023004 => VulkanError.VideoProfileCodecNotSupportedKhr,
        -1000023005 => VulkanError.VideoStdVersionNotSupportedKhr,
        -1000158000 => VulkanError.InvalidDrmFormatModifierPlaneLayoutExt,
        -1000174001 => VulkanError.NotPermittedKhr,
        -1000255000 => VulkanError.FullScreenExclusiveModeLostExt,
        -1000338000 => VulkanError.CompressionExhaustedExt,
        else => if (value < 0) VulkanError.Unknown else value,
    };
}

pub fn acquireAll(
    allocator: Allocator,
    comptime Output: type,
    comptime function: anytype,
    args: anytype,
) ![]Output {
    var count: u32 = undefined;

    try err(@call(.auto, function, args ++ .{ &count, null }));

    const buffer = try allocator.alloc(Output, count);

    const ptr: *Output = @ptrCast(buffer.ptr);

    try err(@call(.auto, function, args ++ .{ &count, ptr }));

    return buffer;
}

pub fn getAll(
    allocator: Allocator,
    comptime Output: type,
    comptime function: anytype,
    args: anytype,
) ![]Output {
    var count: u32 = undefined;

    const first = @call(.auto, function, args ++ .{ &count, null });

    assert(@TypeOf(first) == void);

    const buffer = try allocator.alloc(Output, count);

    const ptr: *Output = @ptrCast(buffer.ptr);

    const second = @call(.auto, function, args ++ .{ &count, ptr });

    assert(@TypeOf(second) == void);

    return buffer;
}

pub fn acquire(
    comptime Output: type,
    comptime function: anytype,
    args: anytype,
) !Output {
    var out: Output = undefined;

    try err(@call(.auto, function, args ++ .{&out}));

    return out;
}

pub fn get(
    comptime Output: type,
    comptime function: anytype,
    args: anytype,
) Output {
    var out: Output = undefined;

    const value = @call(.auto, function, args ++ .{&out});

    assert(@TypeOf(value) == void);

    return out;
}

pub fn toMemoryType(
    types: []const c.VkMemoryType,
    filter: u32,
    props: c.VkMemoryPropertyFlags,
) !u32 {
    return for (types, 0..) |t, index| {
        const i: u32 = @intCast(index);
        if ((filter & (@as(u32, 1) <<| i)) > 0 and (t.propertyFlags & props) == props) {
            break i;
        }
    } else error.NoSuitableMemoryType;
}
