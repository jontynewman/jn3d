const Buffer = @This();

const vulkan = @import("../vulkan.zig");
const c = @import("root").c;

buffer: c.VkBuffer,
memory: c.VkDeviceMemory,

pub fn init(
    device: c.VkDevice,
    size: c.VkDeviceSize,
    usage: c.VkBufferUsageFlags,
    types: []const c.VkMemoryType,
    flags: c.VkMemoryPropertyFlags,
) !Buffer {
    const buffer = try vulkan.acquire(
        c.VkBuffer,
        c.vkCreateBuffer,
        .{
            device,
            &c.VkBufferCreateInfo{
                .sType = c.VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
                .size = size,
                .usage = usage,
                .sharingMode = c.VK_SHARING_MODE_EXCLUSIVE,
                .queueFamilyIndexCount = 0,
                .pQueueFamilyIndices = null,
                .flags = 0,
                .pNext = null,
            },
            null,
        },
    );

    const requirements = vulkan.get(
        c.VkMemoryRequirements,
        c.vkGetBufferMemoryRequirements,
        .{ device, buffer },
    );

    const memory = try vulkan.acquire(
        c.VkDeviceMemory,
        c.vkAllocateMemory,
        .{
            device,
            &c.VkMemoryAllocateInfo{
                .sType = c.VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
                .allocationSize = requirements.size,
                .memoryTypeIndex = try vulkan.toMemoryType(
                    types,
                    requirements.memoryTypeBits,
                    flags,
                ),
                .pNext = null,
            },
            null,
        },
    );

    try vulkan.err(c.vkBindBufferMemory(device, buffer, memory, 0));

    return .{
        .buffer = buffer,
        .memory = memory,
    };
}

pub fn deinit(buffer: Buffer, device: c.VkDevice) void {
    defer c.vkDestroyBuffer(device, buffer.buffer, null);
    defer c.vkFreeMemory(device, buffer.memory, null);
}
