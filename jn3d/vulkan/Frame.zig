const Frame = @This();

const math = @import("std").math;

const vulkan = @import("../vulkan.zig");
const c = @import("root").c;

image: c.VkSemaphore,
render: c.VkSemaphore,
flight: c.VkFence,

pub fn init(device: c.VkDevice) !Frame {
    const info = c.VkSemaphoreCreateInfo{
        .sType = c.VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
        .flags = 0,
        .pNext = null,
    };

    return Frame{
        .image = try vulkan.acquire(c.VkSemaphore, c.vkCreateSemaphore, .{ device, &info, null }),
        .render = try vulkan.acquire(c.VkSemaphore, c.vkCreateSemaphore, .{ device, &info, null }),
        .flight = try vulkan.acquire(c.VkFence, c.vkCreateFence, .{ device, &c.VkFenceCreateInfo{
            .sType = c.VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
            .flags = c.VK_FENCE_CREATE_SIGNALED_BIT,
            .pNext = null,
        }, null }),
    };
}

pub fn next(frame: *const Frame, device: c.VkDevice, swapchain: c.VkSwapchainKHR) !u32 {
    try vulkan.err(c.vkWaitForFences(device, 1, &frame.flight, c.VK_FALSE, math.maxInt(u64)));

    return try vulkan.acquire(
        u32,
        c.vkAcquireNextImageKHR,
        .{ device, swapchain, math.maxInt(u64), frame.image, null },
    );
}

pub fn preflight(frame: *const Frame, device: c.VkDevice) !void {
    try vulkan.err(c.vkResetFences(device, 1, &frame.flight));
}

pub fn submit(frame: *const Frame, queue: c.VkQueue, buffer: c.VkCommandBuffer) !void {
    try vulkan.err(
        c.vkQueueSubmit(
            queue,
            1,
            &c.VkSubmitInfo{
                .sType = c.VK_STRUCTURE_TYPE_SUBMIT_INFO,
                .waitSemaphoreCount = 1,
                .pWaitSemaphores = &frame.image,
                .pWaitDstStageMask = @as(*const u32, @ptrCast(&c.VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT)),
                .commandBufferCount = 1,
                .pCommandBuffers = &buffer,
                .signalSemaphoreCount = 1,
                .pSignalSemaphores = &frame.render,
                .pNext = null,
            },
            frame.flight,
        ),
    );
}

pub fn present(frame: *const Frame, queue: c.VkQueue, swapchain: c.VkSwapchainKHR, i: u32) !c.VkResult {
    return try vulkan.result(
        c.vkQueuePresentKHR(
            queue,
            &c.VkPresentInfoKHR{
                .sType = c.VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
                .waitSemaphoreCount = 1,
                .pWaitSemaphores = &frame.render,
                .swapchainCount = 1,
                .pSwapchains = &swapchain,
                .pImageIndices = &i,
                .pResults = null,
                .pNext = null,
            },
        ),
    );
}

pub fn deinit(frame: *const Frame, device: c.VkDevice) void {
    defer c.vkDestroySemaphore(device, frame.image, null);

    defer c.vkDestroySemaphore(device, frame.render, null);

    defer c.vkDestroyFence(device, frame.flight, null);
}
