const Swapchain = @This();

const std = @import("std");
const Allocator = std.mem.Allocator;
const math = std.math;

const vulkan = @import("../vulkan.zig");
const c = @import("root").c;
const Image = @import("Image.zig");

allocator: Allocator,
swapchain: c.VkSwapchainKHR,
extent: c.VkExtent2D,
images: []c.VkImage,
views: []c.VkImageView,
depth: Image,
depth_view: c.VkImageView,
framebuffers: []c.VkFramebuffer,

pub fn init(
    allocator: Allocator,
    device: c.VkDevice,
    graphics: u32,
    present: u32,
    surface: c.VkSurfaceKHR,
    format: c.VkSurfaceFormatKHR,
    size: c.VkExtent2D,
    capabilities: c.VkSurfaceCapabilitiesKHR,
    mode: c.VkPresentModeKHR,
    depth_format: u32,
    memory_types: []const c.VkMemoryType,
    renderpass: c.VkRenderPass,
) !Swapchain {
    const extent = toExtent(capabilities, size);

    const swapchain = try createSwapchain(device, graphics, present, surface, format, extent, capabilities, mode);

    const images = try getImages(allocator, device, swapchain);

    const views = try allocator.alloc(c.VkImageView, images.len);

    for (views, 0..) |*view, i| {
        view.* = try vulkan.acquire(
            c.VkImageView,
            c.vkCreateImageView,
            .{
                device,
                &c.VkImageViewCreateInfo{
                    .sType = c.VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
                    .image = images[i],
                    .viewType = c.VK_IMAGE_VIEW_TYPE_2D,
                    .format = format.format,
                    .components = .{
                        .r = c.VK_COMPONENT_SWIZZLE_IDENTITY,
                        .g = c.VK_COMPONENT_SWIZZLE_IDENTITY,
                        .b = c.VK_COMPONENT_SWIZZLE_IDENTITY,
                        .a = c.VK_COMPONENT_SWIZZLE_IDENTITY,
                    },
                    .subresourceRange = .{
                        .aspectMask = c.VK_IMAGE_ASPECT_COLOR_BIT,
                        .baseMipLevel = 0,
                        .levelCount = 1,
                        .baseArrayLayer = 0,
                        .layerCount = 1, // Likely needs to be 2 for VR.
                    },
                    .flags = 0,
                    .pNext = null,
                },
                null,
            },
        );
    }

    const depth = try Image.init(
        device,
        extent.width,
        extent.height,
        depth_format,
        c.VK_IMAGE_TILING_OPTIMAL,
        c.VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
        memory_types,
        c.VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
    );

    const depth_view = try createImageView(device, depth.image, depth_format);

    const framebuffers = try allocator.alloc(c.VkFramebuffer, views.len);

    for (framebuffers, 0..) |*framebuffer, i| {
        framebuffer.* = try createFramebuffer(device, renderpass, views[i], depth_view, extent);
    }

    return Swapchain{
        .allocator = allocator,
        .swapchain = swapchain,
        .extent = extent,
        .images = images,
        .views = views,
        .depth = depth,
        .depth_view = depth_view,
        .framebuffers = framebuffers,
    };
}

fn toExtent(capabilities: c.VkSurfaceCapabilitiesKHR, fallback: c.VkExtent2D) c.VkExtent2D {
    const current = capabilities.currentExtent;
    const limit = math.maxInt(u32);

    if (current.width < limit and current.height < limit) {
        return current;
    }

    const min = capabilities.minImageExtent;
    const max = capabilities.maxImageExtent;

    return clamp(fallback, min, max);
}

fn clamp(unclamped: c.VkExtent2D, min: c.VkExtent2D, max: c.VkExtent2D) c.VkExtent2D {
    var clamped = unclamped;

    clamped.width = math.clamp(clamped.width, min.width, max.width);
    clamped.height = math.clamp(clamped.height, min.height, max.height);

    return clamped;
}

fn createSwapchain(
    device: c.VkDevice,
    graphics: u32,
    present: u32,
    surface: c.VkSurfaceKHR,
    format: c.VkSurfaceFormatKHR,
    extent: c.VkExtent2D,
    capabilities: c.VkSurfaceCapabilitiesKHR,
    mode: c.VkPresentModeKHR,
) !c.VkSwapchainKHR {
    const concurrent: c.VkSharingMode = c.VK_SHARING_MODE_CONCURRENT;
    const exclusive: c.VkSharingMode = c.VK_SHARING_MODE_EXCLUSIVE;
    const min = capabilities.minImageCount + 1;
    const max = capabilities.maxImageCount;

    return try vulkan.acquire(
        c.VkSwapchainKHR,
        c.vkCreateSwapchainKHR,
        .{
            device,
            &c.VkSwapchainCreateInfoKHR{
                .sType = c.VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
                .surface = surface,
                .minImageCount = if (max > 0 and min > max) max else min,
                .imageFormat = format.format,
                .imageColorSpace = format.colorSpace,
                .imageExtent = extent,
                .imageArrayLayers = 1, // This'll likely need to be 2 for VR.
                .imageUsage = c.VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
                .imageSharingMode = if (graphics == present) exclusive else concurrent,
                .queueFamilyIndexCount = if (graphics == present) 0 else 2,
                .pQueueFamilyIndices = if (graphics == present) null else &[_]u32{
                    graphics,
                    present,
                },
                .preTransform = capabilities.currentTransform,
                .compositeAlpha = c.VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
                .presentMode = mode,
                .clipped = c.VK_TRUE,
                .oldSwapchain = null,
                .flags = 0,
                .pNext = null,
            },
            null,
        },
    );
}

fn getImages(allocator: Allocator, device: c.VkDevice, swapchain: c.VkSwapchainKHR) ![]c.VkImage {
    return try vulkan.acquireAll(
        allocator,
        c.VkImage,
        c.vkGetSwapchainImagesKHR,
        .{ device, swapchain },
    );
}

fn createImageView(device: c.VkDevice, image: c.VkImage, format: c.VkFormat) !c.VkImageView {
    return try vulkan.acquire(
        c.VkImageView,
        c.vkCreateImageView,
        .{
            device,
            &c.VkImageViewCreateInfo{
                .sType = c.VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
                .image = image,
                .viewType = c.VK_IMAGE_VIEW_TYPE_2D,
                .format = format,
                .components = .{
                    .r = c.VK_COMPONENT_SWIZZLE_IDENTITY,
                    .g = c.VK_COMPONENT_SWIZZLE_IDENTITY,
                    .b = c.VK_COMPONENT_SWIZZLE_IDENTITY,
                    .a = c.VK_COMPONENT_SWIZZLE_IDENTITY,
                },
                .subresourceRange = .{
                    .aspectMask = c.VK_IMAGE_ASPECT_DEPTH_BIT,
                    .baseMipLevel = 0,
                    .levelCount = 1,
                    .baseArrayLayer = 0,
                    .layerCount = 1, // Likely needs to be 2 for VR.
                },
                .flags = 0,
                .pNext = null,
            },
            null,
        },
    );
}

fn createFramebuffer(
    device: c.VkDevice,
    renderpass: c.VkRenderPass,
    view: c.VkImageView,
    depth: c.VkImageView,
    extent: c.VkExtent2D,
) !c.VkFramebuffer {
    const attachments = [_]c.VkImageView{
        view,
        depth,
    };

    return try vulkan.acquire(
        c.VkFramebuffer,
        c.vkCreateFramebuffer,
        .{
            device,
            &c.VkFramebufferCreateInfo{
                .sType = c.VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
                .renderPass = renderpass,
                .attachmentCount = attachments.len,
                .pAttachments = &attachments,
                .width = extent.width,
                .height = extent.height,
                .layers = 1,
                .flags = 0,
                .pNext = null,
            },
            null,
        },
    );
}

pub fn reinit(
    swapchain: *Swapchain,
    device: c.VkDevice,
    graphics: u32,
    present: u32,
    surface: c.VkSurfaceKHR,
    format: c.VkSurfaceFormatKHR,
    size: c.VkExtent2D,
    capabilities: c.VkSurfaceCapabilitiesKHR,
    mode: c.VkPresentModeKHR,
    depth_format: u32,
    memory_types: []const c.VkMemoryType,
    renderpass: c.VkRenderPass,
) !void {
    swapchain.destroy(device);

    swapchain.extent = toExtent(capabilities, size);

    swapchain.swapchain = try createSwapchain(device, graphics, present, surface, format, swapchain.extent, capabilities, mode);

    swapchain.images = try getImages(swapchain.allocator, device, swapchain.swapchain);

    for (swapchain.views, 0..) |*view, i| {
        view.* = try vulkan.acquire(
            c.VkImageView,
            c.vkCreateImageView,
            .{
                device,
                &c.VkImageViewCreateInfo{
                    .sType = c.VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
                    .image = swapchain.images[i],
                    .viewType = c.VK_IMAGE_VIEW_TYPE_2D,
                    .format = format.format,
                    .components = .{
                        .r = c.VK_COMPONENT_SWIZZLE_IDENTITY,
                        .g = c.VK_COMPONENT_SWIZZLE_IDENTITY,
                        .b = c.VK_COMPONENT_SWIZZLE_IDENTITY,
                        .a = c.VK_COMPONENT_SWIZZLE_IDENTITY,
                    },
                    .subresourceRange = .{
                        .aspectMask = c.VK_IMAGE_ASPECT_COLOR_BIT,
                        .baseMipLevel = 0,
                        .levelCount = 1,
                        .baseArrayLayer = 0,
                        .layerCount = 1, // Likely needs to be 2 for VR.
                    },
                    .flags = 0,
                    .pNext = null,
                },
                null,
            },
        );
    }

    swapchain.depth = try Image.init(
        device,
        swapchain.extent.width,
        swapchain.extent.height,
        depth_format,
        c.VK_IMAGE_TILING_OPTIMAL,
        c.VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
        memory_types,
        c.VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
    );

    swapchain.depth_view = try createImageView(device, swapchain.depth.image, depth_format);

    for (swapchain.framebuffers, 0..) |*framebuffer, i| {
        framebuffer.* = try createFramebuffer(device, renderpass, swapchain.views[i], swapchain.depth_view, swapchain.extent);
    }
}

pub fn deinit(swapchain: *const Swapchain, device: c.VkDevice) void {
    const allocator = swapchain.allocator;

    defer allocator.free(swapchain.images);

    defer allocator.free(swapchain.views);

    defer allocator.free(swapchain.framebuffers);

    defer swapchain.destroy(device);
}

fn destroy(swapchain: *const Swapchain, device: c.VkDevice) void {
    defer c.vkDestroySwapchainKHR(device, swapchain.swapchain, null);

    for (swapchain.views) |view| {
        defer c.vkDestroyImageView(device, view, null);
    }

    defer swapchain.depth.deinit(device);

    defer c.vkDestroyImageView(device, swapchain.depth_view, null);

    for (swapchain.framebuffers) |framebuffer| {
        defer c.vkDestroyFramebuffer(device, framebuffer, null);
    }
}
