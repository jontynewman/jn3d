const Vulkan = @This();

const c = @import("root").c;

const std = @import("std");
const Allocator = std.mem.Allocator;
const AutoArrayHashMap = std.AutoArrayHashMap;
const BufSet = std.BufSet;
const math = std.math;
const now = std.time.milliTimestamp;

const jn3d = @import("../jn3d.zig");

const UniformBufferObject = jn3d.UniformBufferObject;
const Vertex = jn3d.Vertex;

const vulkan = jn3d.vulkan;
const physical_device = vulkan.physical_device;
const VulkanError = vulkan.VulkanError;

const Buffer = vulkan.Buffer;
const Frame = vulkan.Frame;
const Image = vulkan.Image;
const QueueFamily = vulkan.QueueFamily;
const Swapchain = vulkan.Swapchain;
const UniformBuffer = vulkan.UniformBuffer;

allocator: Allocator,
surface: c.VkSurfaceKHR,
physical: c.VkPhysicalDevice,
device: c.VkDevice,
graphics: QueueFamily,
present: QueueFamily,
mode: u32,
format: c.VkSurfaceFormatKHR,
swapchain: Swapchain,
vert: c.VkShaderModule,
frag: c.VkShaderModule,
descriptor_set_layout: c.VkDescriptorSetLayout,
layout: c.VkPipelineLayout,
renderpass: c.VkRenderPass,
pipeline: c.VkPipeline,
pool: c.VkCommandPool,
texture: Image,
texture_view: c.VkImageView,
sampler: c.VkSampler,
vertex_buffer: Buffer,
index_buffer: Buffer,
uniform_buffers: []UniformBuffer,
descriptor_pool: c.VkDescriptorPool,
descriptor_sets: []c.VkDescriptorSet,
buffers: []c.VkCommandBuffer,
frames: []Frame,
i: u32,
resize: bool,
start: i128, //TODO migrate to main.zig
indices_len: usize,

pub fn init(
    allocator: Allocator,
    instance: c.VkInstance,
    surface: c.VkSurfaceKHR,
    size: c.VkExtent2D,
    count: u32,
    vertices: []const Vertex,
    indices: []const u32,
    texture_data: ?[]const u8,
) !Vulkan {
    var physical: c.VkPhysicalDevice = undefined;
    var graphics: QueueFamily = undefined;
    var present: QueueFamily = undefined;
    var format: c.VkSurfaceFormatKHR = undefined;
    var mode: c.VkPresentModeKHR = undefined;

    const physicals = try vulkan.acquireAll(
        allocator,
        c.VkPhysicalDevice,
        c.vkEnumeratePhysicalDevices,
        .{instance},
    );
    defer allocator.free(physicals);

    const device = try for (physicals) |p| {
        physical = p;

        const families = try vulkan.getAll(
            allocator,
            c.VkQueueFamilyProperties,
            c.vkGetPhysicalDeviceQueueFamilyProperties,
            .{physical},
        );

        const g = for (families, 0..) |family, i| {
            if (0 != (family.queueFlags & c.VK_QUEUE_GRAPHICS_BIT)) {
                break i;
            }
        } else continue;
        graphics.index = @as(u32, @intCast(g));

        const s = for (families, 0..) |_, i| {
            const support = try vulkan.acquire(
                c.VkBool32,
                c.vkGetPhysicalDeviceSurfaceSupportKHR,
                .{ physical, @as(u32, @intCast(i)), surface },
            );

            if (c.VK_TRUE == support) {
                break i;
            }
        } else continue;
        present.index = @as(u32, @intCast(s));

        const formats = try vulkan.acquireAll(
            allocator,
            c.VkSurfaceFormatKHR,
            c.vkGetPhysicalDeviceSurfaceFormatsKHR,
            .{ physical, surface },
        );
        format = try toFormat(formats);

        const modes = try vulkan.acquireAll(
            allocator,
            c.VkPresentModeKHR,
            c.vkGetPhysicalDeviceSurfacePresentModesKHR,
            .{ physical, surface },
        );

        const m = toPresentMode(modes);
        mode = @as(u32, @intCast(m));

        if (!try hasRequiredExtensions(allocator, physical)) {
            continue;
        }

        var infos = try toDeviceQueueCreateInfos(allocator, graphics.index, present.index);
        defer infos.deinit();

        const extensions = [_][]const u8{
            c.VK_KHR_SWAPCHAIN_EXTENSION_NAME,
        };

        const features = vulkan.get(c.VkPhysicalDeviceFeatures, c.vkGetPhysicalDeviceFeatures, .{physical});

        const device = try vulkan.acquire(
            c.VkDevice,
            c.vkCreateDevice,
            .{
                physical,
                &c.VkDeviceCreateInfo{
                    .sType = c.VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
                    .pQueueCreateInfos = infos.values().ptr,
                    .queueCreateInfoCount = @as(u32, @intCast(infos.count())),
                    .ppEnabledExtensionNames = @as(*const *const u8, @ptrCast(&extensions)),
                    .enabledExtensionCount = extensions.len,
                    .ppEnabledLayerNames = @as(*const *const u8, @ptrCast(&vulkan.layers)),
                    .enabledLayerCount = @as(u32, @intCast(vulkan.layers.len)),
                    .pEnabledFeatures = &features,
                    .flags = 0,
                    .pNext = null,
                },
                null,
            },
        );

        break device;
    } else error.NoSupportedDevice;

    graphics.queue = vulkan.get(
        c.VkQueue,
        c.vkGetDeviceQueue,
        .{ device, graphics.index, 0 },
    );

    present.queue = vulkan.get(
        c.VkQueue,
        c.vkGetDeviceQueue,
        .{ device, present.index, 0 },
    );

    const vert = try toShaderModule(device, @embedFile("shaders/vert.spv"));
    const frag = try toShaderModule(device, @embedFile("shaders/frag.spv"));

    const stages = [_]c.VkPipelineShaderStageCreateInfo{
        .{
            .sType = c.VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
            .stage = c.VK_SHADER_STAGE_VERTEX_BIT,
            .module = vert,
            .pName = "main",
            .pSpecializationInfo = null,
            .flags = 0,
            .pNext = null,
        },
        .{
            .sType = c.VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
            .stage = c.VK_SHADER_STAGE_FRAGMENT_BIT,
            .module = frag,
            .pName = "main",
            .pSpecializationInfo = null,
            .flags = 0,
            .pNext = null,
        },
    };

    const descriptor_set_layout = try vulkan.acquire(
        c.VkDescriptorSetLayout,
        c.vkCreateDescriptorSetLayout,
        .{
            device,
            &c.VkDescriptorSetLayoutCreateInfo{
                .sType = c.VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
                .bindingCount = 2,
                .pBindings = &[_]c.VkDescriptorSetLayoutBinding{
                    .{
                        .binding = 0,
                        .descriptorType = c.VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                        .descriptorCount = 1,
                        .stageFlags = c.VK_SHADER_STAGE_VERTEX_BIT,
                        .pImmutableSamplers = null,
                    },
                    .{
                        .binding = 1,
                        .descriptorType = c.VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                        .descriptorCount = 1,
                        .stageFlags = c.VK_SHADER_STAGE_FRAGMENT_BIT,
                        .pImmutableSamplers = null,
                    },
                },
                .flags = 0,
                .pNext = null,
            },
            null,
        },
    );

    const layout = try vulkan.acquire(
        c.VkPipelineLayout,
        c.vkCreatePipelineLayout,
        .{
            device,
            &c.VkPipelineLayoutCreateInfo{
                .sType = c.VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
                .setLayoutCount = 1,
                .pSetLayouts = &descriptor_set_layout,
                .pushConstantRangeCount = 0,
                .pPushConstantRanges = null,
                .flags = 0,
                .pNext = null,
            },
            null,
        },
    );

    const depth_format = c.VK_FORMAT_D32_SFLOAT; //TODO Compute "best" format.

    const memory_properties = vulkan.get(
        c.VkPhysicalDeviceMemoryProperties,
        c.vkGetPhysicalDeviceMemoryProperties,
        .{physical},
    );

    const memory_types = memory_properties.memoryTypes[0..memory_properties.memoryTypeCount];

    const attachments = [_]c.VkAttachmentDescription{
        .{
            .format = format.format,
            .samples = c.VK_SAMPLE_COUNT_1_BIT,
            .loadOp = c.VK_ATTACHMENT_LOAD_OP_CLEAR,
            .storeOp = c.VK_ATTACHMENT_STORE_OP_STORE,
            .stencilLoadOp = c.VK_ATTACHMENT_LOAD_OP_DONT_CARE,
            .stencilStoreOp = c.VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .initialLayout = c.VK_IMAGE_LAYOUT_UNDEFINED,
            .finalLayout = c.VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
            .flags = 0,
        },
        .{
            .format = depth_format,
            .samples = c.VK_SAMPLE_COUNT_1_BIT,
            .loadOp = c.VK_ATTACHMENT_LOAD_OP_CLEAR,
            .storeOp = c.VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .stencilLoadOp = c.VK_ATTACHMENT_LOAD_OP_DONT_CARE,
            .stencilStoreOp = c.VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .initialLayout = c.VK_IMAGE_LAYOUT_UNDEFINED,
            .finalLayout = c.VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
            .flags = 0,
        },
    };

    const renderpass = try vulkan.acquire(
        c.VkRenderPass,
        c.vkCreateRenderPass,
        .{
            device,
            &c.VkRenderPassCreateInfo{
                .sType = c.VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
                .attachmentCount = attachments.len,
                .pAttachments = &attachments,
                .subpassCount = 1,
                .pSubpasses = &c.VkSubpassDescription{
                    .pipelineBindPoint = c.VK_PIPELINE_BIND_POINT_GRAPHICS,
                    .colorAttachmentCount = 1,
                    .pColorAttachments = &c.VkAttachmentReference{
                        .attachment = 0,
                        .layout = c.VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                    },
                    .inputAttachmentCount = 0,
                    .pInputAttachments = null,
                    .pResolveAttachments = null,
                    .pDepthStencilAttachment = &c.VkAttachmentReference{
                        .attachment = 1,
                        .layout = c.VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
                    },
                    .preserveAttachmentCount = 0,
                    .pPreserveAttachments = null,
                    .flags = 0,
                },
                .dependencyCount = 1,
                .pDependencies = &c.VkSubpassDependency{
                    .srcSubpass = c.VK_SUBPASS_EXTERNAL,
                    .dstSubpass = 0,
                    .srcStageMask = c.VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | c.VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT,
                    .dstStageMask = c.VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | c.VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT,
                    .srcAccessMask = 0,
                    .dstAccessMask = c.VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | c.VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
                    .dependencyFlags = 0,
                },
                .flags = 0,
                .pNext = null,
            },
            null,
        },
    );

    const swapchain = try Swapchain.init(
        allocator,
        device,
        graphics.index,
        present.index,
        surface,
        format,
        size,
        try vulkan.acquire(
            c.VkSurfaceCapabilitiesKHR,
            c.vkGetPhysicalDeviceSurfaceCapabilitiesKHR,
            .{ physical, surface },
        ),
        mode,
        depth_format,
        memory_types,
        renderpass,
    );

    const states = [_]c.VkDynamicState{
        c.VK_DYNAMIC_STATE_VIEWPORT,
        c.VK_DYNAMIC_STATE_SCISSOR,
    };

    const attributes = Vertex.attributes();

    const pipeline = try vulkan.acquire(
        c.VkPipeline,
        c.vkCreateGraphicsPipelines,
        .{
            device,
            null,
            1,
            &c.VkGraphicsPipelineCreateInfo{
                .sType = c.VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
                .stageCount = @as(u32, @intCast(stages.len)),
                .pStages = &stages,
                .pVertexInputState = &c.VkPipelineVertexInputStateCreateInfo{
                    .sType = c.VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
                    .vertexBindingDescriptionCount = 1,
                    .pVertexBindingDescriptions = &Vertex.binding(),
                    .vertexAttributeDescriptionCount = @as(u32, @intCast(attributes.len)),
                    .pVertexAttributeDescriptions = attributes.ptr,
                    .flags = 0,
                    .pNext = null,
                },
                .pInputAssemblyState = &c.VkPipelineInputAssemblyStateCreateInfo{
                    .sType = c.VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
                    .topology = c.VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
                    .primitiveRestartEnable = c.VK_FALSE,
                    .flags = 0,
                    .pNext = null,
                },
                .pViewportState = &c.VkPipelineViewportStateCreateInfo{
                    .sType = c.VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
                    .viewportCount = 1,
                    .pViewports = null,
                    .scissorCount = 1,
                    .pScissors = null,
                    .flags = 0,
                    .pNext = null,
                },
                .pRasterizationState = &c.VkPipelineRasterizationStateCreateInfo{
                    .sType = c.VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
                    .depthClampEnable = c.VK_FALSE,
                    .rasterizerDiscardEnable = c.VK_FALSE,
                    .polygonMode = c.VK_POLYGON_MODE_FILL,
                    .lineWidth = 1.0,
                    .cullMode = c.VK_CULL_MODE_BACK_BIT,
                    .frontFace = c.VK_FRONT_FACE_COUNTER_CLOCKWISE,
                    .depthBiasEnable = c.VK_FALSE,
                    .depthBiasConstantFactor = 0,
                    .depthBiasClamp = 0,
                    .depthBiasSlopeFactor = 0,
                    .flags = 0,
                    .pNext = null,
                },
                .pMultisampleState = &c.VkPipelineMultisampleStateCreateInfo{
                    .sType = c.VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
                    .sampleShadingEnable = c.VK_FALSE,
                    .rasterizationSamples = c.VK_SAMPLE_COUNT_1_BIT,
                    .minSampleShading = 1.0,
                    .pSampleMask = null,
                    .alphaToCoverageEnable = c.VK_FALSE,
                    .alphaToOneEnable = c.VK_FALSE,
                    .flags = 0,
                    .pNext = null,
                },
                .pDepthStencilState = &c.VkPipelineDepthStencilStateCreateInfo{
                    .sType = c.VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
                    .depthTestEnable = c.VK_TRUE,
                    .depthWriteEnable = c.VK_TRUE,
                    .depthCompareOp = c.VK_COMPARE_OP_LESS,
                    .depthBoundsTestEnable = c.VK_FALSE,
                    .stencilTestEnable = c.VK_FALSE,
                    .front = .{
                        .failOp = c.VK_STENCIL_OP_KEEP,
                        .passOp = c.VK_STENCIL_OP_KEEP,
                        .depthFailOp = c.VK_STENCIL_OP_KEEP,
                        .compareOp = c.VK_COMPARE_OP_NEVER,
                        .compareMask = 0,
                        .writeMask = 0,
                        .reference = 0,
                    },
                    .back = .{
                        .failOp = c.VK_STENCIL_OP_KEEP,
                        .passOp = c.VK_STENCIL_OP_KEEP,
                        .depthFailOp = c.VK_STENCIL_OP_KEEP,
                        .compareOp = c.VK_COMPARE_OP_NEVER,
                        .compareMask = 0,
                        .writeMask = 0,
                        .reference = 0,
                    },
                    .minDepthBounds = 0.0,
                    .maxDepthBounds = 1.0,
                    .flags = 0,
                    .pNext = null,
                },
                .pColorBlendState = &c.VkPipelineColorBlendStateCreateInfo{
                    .sType = c.VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
                    .logicOpEnable = c.VK_FALSE,
                    .logicOp = c.VK_LOGIC_OP_COPY,
                    .attachmentCount = 1,
                    .pAttachments = &c.VkPipelineColorBlendAttachmentState{
                        .colorWriteMask = c.VK_COLOR_COMPONENT_R_BIT | c.VK_COLOR_COMPONENT_G_BIT | c.VK_COLOR_COMPONENT_B_BIT | c.VK_COLOR_COMPONENT_A_BIT,
                        .blendEnable = c.VK_FALSE,
                        .srcColorBlendFactor = c.VK_BLEND_FACTOR_ONE,
                        .dstColorBlendFactor = c.VK_BLEND_FACTOR_ZERO,
                        .colorBlendOp = c.VK_BLEND_OP_ADD,
                        .srcAlphaBlendFactor = c.VK_BLEND_FACTOR_ONE,
                        .dstAlphaBlendFactor = c.VK_BLEND_FACTOR_ZERO,
                        .alphaBlendOp = c.VK_BLEND_OP_ADD,
                    },
                    .blendConstants = [_]f32{ 0.0, 0.0, 0.0, 0.0 },
                    .flags = 0,
                    .pNext = null,
                },
                .pDynamicState = &c.VkPipelineDynamicStateCreateInfo{
                    .sType = c.VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
                    .dynamicStateCount = states.len,
                    .pDynamicStates = &states,
                    .flags = 0,
                    .pNext = null,
                },
                .layout = layout,
                .renderPass = renderpass,
                .subpass = 0,
                .basePipelineHandle = null,
                .basePipelineIndex = -1,
                .pTessellationState = null,
                .flags = 0,
                .pNext = null,
            },
            null,
        },
    );

    const pool = try vulkan.acquire(
        c.VkCommandPool,
        c.vkCreateCommandPool,
        .{
            device,
            &c.VkCommandPoolCreateInfo{
                .sType = c.VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
                .flags = c.VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
                .queueFamilyIndex = graphics.index,
                .pNext = null,
            },
            null,
        },
    );

    try transitionImageLayout(
        device,
        pool,
        graphics.queue,
        swapchain.depth.image,
        c.VK_IMAGE_LAYOUT_UNDEFINED,
        c.VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
        c.VK_IMAGE_ASPECT_DEPTH_BIT,
        c.VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
        c.VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT,
        0,
        c.VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | c.VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
    );

    const jn3d_image = try jn3d.Image.init(texture_data orelse @embedFile("../textures/default.png"));
    defer jn3d_image.deinit();

    const image_size = jn3d_image.width * jn3d_image.height * 4;

    const image_buffer = try Buffer.init(
        device,
        image_size,
        c.VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        memory_types,
        c.VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | c.VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
    );
    defer image_buffer.deinit(device);

    try mapMemory(device, jn3d_image.pixels, image_buffer.memory, image_size);

    const texture = try Image.init(
        device,
        @as(u32, @intCast(jn3d_image.width)),
        @as(u32, @intCast(jn3d_image.height)),
        c.VK_FORMAT_R8G8B8A8_SRGB,
        c.VK_IMAGE_TILING_OPTIMAL,
        c.VK_IMAGE_USAGE_TRANSFER_DST_BIT | c.VK_IMAGE_USAGE_SAMPLED_BIT,
        memory_types,
        c.VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
    );

    const texture_view = try vulkan.acquire(
        c.VkImageView,
        c.vkCreateImageView,
        .{
            device,
            &c.VkImageViewCreateInfo{
                .sType = c.VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
                .image = texture.image,
                .viewType = c.VK_IMAGE_VIEW_TYPE_2D,
                .format = c.VK_FORMAT_R8G8B8A8_SRGB,
                .components = .{
                    .r = c.VK_COMPONENT_SWIZZLE_IDENTITY,
                    .g = c.VK_COMPONENT_SWIZZLE_IDENTITY,
                    .b = c.VK_COMPONENT_SWIZZLE_IDENTITY,
                    .a = c.VK_COMPONENT_SWIZZLE_IDENTITY,
                },
                .subresourceRange = .{
                    .aspectMask = c.VK_IMAGE_ASPECT_COLOR_BIT,
                    .baseMipLevel = 0,
                    .levelCount = 1,
                    .baseArrayLayer = 0,
                    .layerCount = 1, // Likely needs to be 2 for VR.
                },
                .flags = 0,
                .pNext = null,
            },
            null,
        },
    );

    try transitionImageLayout(
        device,
        pool,
        graphics.queue,
        texture.image,
        c.VK_IMAGE_LAYOUT_UNDEFINED,
        c.VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        c.VK_IMAGE_ASPECT_COLOR_BIT,
        c.VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
        c.VK_PIPELINE_STAGE_TRANSFER_BIT,
        0,
        c.VK_ACCESS_TRANSFER_WRITE_BIT,
    );

    try copyBufferToImage(
        device,
        pool,
        graphics.queue,
        image_buffer.buffer,
        texture.image,
        @as(u32, @intCast(jn3d_image.width)),
        @as(u32, @intCast(jn3d_image.height)),
    );

    try transitionImageLayout(
        device,
        pool,
        graphics.queue,
        texture.image,
        c.VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        c.VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
        c.VK_IMAGE_ASPECT_COLOR_BIT,
        c.VK_PIPELINE_STAGE_TRANSFER_BIT,
        c.VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        c.VK_ACCESS_TRANSFER_WRITE_BIT,
        c.VK_ACCESS_SHADER_READ_BIT,
    );

    const sampler = try vulkan.acquire(
        c.VkSampler,
        c.vkCreateSampler,
        .{
            device,
            &c.VkSamplerCreateInfo{
                .sType = c.VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
                .magFilter = c.VK_FILTER_LINEAR,
                .minFilter = c.VK_FILTER_LINEAR,
                .addressModeU = c.VK_SAMPLER_ADDRESS_MODE_REPEAT,
                .addressModeV = c.VK_SAMPLER_ADDRESS_MODE_REPEAT,
                .addressModeW = c.VK_SAMPLER_ADDRESS_MODE_REPEAT,
                .anisotropyEnable = c.VK_FALSE, //TODO Enable anisotropy
                .maxAnisotropy = 1.0, //TODO Determine max anisotropy from device
                .borderColor = c.VK_BORDER_COLOR_INT_OPAQUE_BLACK,
                .unnormalizedCoordinates = c.VK_FALSE,
                .compareEnable = c.VK_FALSE,
                .compareOp = c.VK_COMPARE_OP_ALWAYS,
                .mipmapMode = c.VK_SAMPLER_MIPMAP_MODE_LINEAR,
                .mipLodBias = 0.0,
                .minLod = 0.0,
                .maxLod = 0.0,
                .pNext = null,
                .flags = 0,
            },
            null,
        },
    );

    const vertices_size = @sizeOf(Vertex) * vertices.len;

    const vertex_buffer = try Buffer.init(
        device,
        vertices_size,
        c.VK_BUFFER_USAGE_TRANSFER_DST_BIT | c.VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
        memory_types,
        c.VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | c.VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
    );

    try stageToCommandBuffer(
        device,
        pool,
        graphics.queue,
        &vertices[0],
        vertex_buffer.buffer,
        vertices_size,
        memory_types,
    );

    const indices_size = @sizeOf(u32) * indices.len;

    const index_buffer = try Buffer.init(
        device,
        indices_size,
        c.VK_BUFFER_USAGE_TRANSFER_DST_BIT | c.VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
        memory_types,
        c.VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | c.VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
    );

    try stageToCommandBuffer(
        device,
        pool,
        graphics.queue,
        &indices[0],
        index_buffer.buffer,
        indices_size,
        memory_types,
    );

    const uniform_buffers = try allocator.alloc(UniformBuffer, count);

    for (uniform_buffers) |*uniform_buffer| {
        uniform_buffer.* = try UniformBuffer.init(device, @sizeOf(UniformBufferObject), memory_types);
    }

    const descriptor_pool = try vulkan.acquire(
        c.VkDescriptorPool,
        c.vkCreateDescriptorPool,
        .{
            device,
            &c.VkDescriptorPoolCreateInfo{
                .sType = c.VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
                .poolSizeCount = 2,
                .pPoolSizes = &[_]c.VkDescriptorPoolSize{
                    .{
                        .type = c.VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                        .descriptorCount = count,
                    },
                    .{
                        .type = c.VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                        .descriptorCount = count,
                    },
                },
                .maxSets = count,
                .flags = 0,
                .pNext = null,
            },
            null,
        },
    );

    const descriptor_set_layouts = try allocator.alloc(c.VkDescriptorSetLayout, count);
    defer allocator.free(descriptor_set_layouts);

    for (descriptor_set_layouts) |*l| {
        l.* = descriptor_set_layout;
    }

    const descriptor_sets = try allocator.alloc(c.VkDescriptorSet, count);

    try vulkan.err(c.vkAllocateDescriptorSets(
        device,
        &c.VkDescriptorSetAllocateInfo{
            .sType = c.VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
            .descriptorPool = descriptor_pool,
            .descriptorSetCount = count,
            .pSetLayouts = descriptor_set_layouts.ptr,
            .pNext = null,
        },
        descriptor_sets.ptr,
    ));

    for (descriptor_sets, 0..) |descriptor_set, i| {
        const descriptor_writes = [_]c.VkWriteDescriptorSet{
            .{
                .sType = c.VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                .dstSet = descriptor_set,
                .dstBinding = 0,
                .dstArrayElement = 0,
                .descriptorType = c.VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                .descriptorCount = 1,
                .pBufferInfo = &c.VkDescriptorBufferInfo{
                    .buffer = uniform_buffers[i].buffer.buffer,
                    .offset = 0,
                    .range = @sizeOf(UniformBufferObject),
                },
                .pImageInfo = null,
                .pTexelBufferView = null,
                .pNext = null,
            },
            .{
                .sType = c.VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                .dstSet = descriptor_set,
                .dstBinding = 1,
                .dstArrayElement = 0,
                .descriptorType = c.VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                .descriptorCount = 1,
                .pBufferInfo = null,
                .pImageInfo = &c.VkDescriptorImageInfo{
                    .imageLayout = c.VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                    .imageView = texture_view,
                    .sampler = sampler,
                },
                .pTexelBufferView = null,
                .pNext = null,
            },
        };

        c.vkUpdateDescriptorSets(device, descriptor_writes.len, &descriptor_writes, 0, null);
    }

    const buffers = try allocator.alloc(c.VkCommandBuffer, count);

    try vulkan.err(c.vkAllocateCommandBuffers(
        device,
        &c.VkCommandBufferAllocateInfo{
            .sType = c.VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
            .commandPool = pool,
            .level = c.VK_COMMAND_BUFFER_LEVEL_PRIMARY,
            .commandBufferCount = count,
            .pNext = null,
        },
        buffers.ptr,
    ));

    const frames = try allocator.alloc(Frame, count);

    for (frames) |*frame| {
        frame.* = try Frame.init(device);
    }

    return Vulkan{
        .allocator = allocator,
        .surface = surface,
        .physical = physical,
        .device = device,
        .graphics = graphics,
        .present = present,
        .mode = mode,
        .format = format,
        .swapchain = swapchain,
        .vert = vert,
        .frag = frag,
        .descriptor_set_layout = descriptor_set_layout,
        .layout = layout,
        .renderpass = renderpass,
        .pipeline = pipeline,
        .pool = pool,
        .texture = texture,
        .texture_view = texture_view,
        .sampler = sampler,
        .vertex_buffer = vertex_buffer,
        .index_buffer = index_buffer,
        .uniform_buffers = uniform_buffers,
        .descriptor_pool = descriptor_pool,
        .descriptor_sets = descriptor_sets,
        .buffers = buffers,
        .frames = frames,
        .i = 0,
        .resize = false,
        .start = now(),
        .indices_len = indices.len,
    };
}

fn hasRequiredExtensions(allocator: Allocator, physical: c.VkPhysicalDevice) !bool {
    var set = BufSet.init(allocator);
    const extensions = [_][]const u8{
        c.VK_KHR_SWAPCHAIN_EXTENSION_NAME,
    };
    defer set.deinit();

    const props = try vulkan.acquireAll(
        allocator,
        c.VkExtensionProperties,
        c.vkEnumerateDeviceExtensionProperties,
        .{ physical, null },
    );
    defer allocator.free(props);

    for (extensions) |extension| {
        try set.insert(extension);
    }

    for (props) |available| {
        const name = &available.extensionName;
        const len = std.mem.sliceTo(name, 0).len;

        set.remove(name[0..len]);
    }

    return 0 == set.count();
}

fn toFormat(formats: []c.VkSurfaceFormatKHR) !c.VkSurfaceFormatKHR {
    const target: struct {
        const Self = @This();

        format: c.VkFormat = c.VK_FORMAT_B8G8R8A8_SRGB,
        colorSpace: c.VkColorSpaceKHR = c.VK_COLOR_SPACE_SRGB_NONLINEAR_KHR,

        pub fn eql(self: *const Self, format: c.VkSurfaceFormatKHR) bool {
            return self.format == format.format and self.colorSpace == format.colorSpace;
        }
    } = .{};

    return if (formats.len > 0) for (formats) |format| {
        if (target.eql(format)) {
            break format;
        }
    } else formats[0] else error.NoFormats;
}

fn toPresentMode(modes: []c.VkPresentModeKHR) c.VkPresentModeKHR {
    return for (modes) |mode| {
        if (c.VK_PRESENT_MODE_MAILBOX_KHR == mode) {
            break mode;
        }
    } else c.VK_PRESENT_MODE_FIFO_KHR;
}

fn toDeviceQueueCreateInfos(
    allocator: Allocator,
    graphics: u32,
    present: u32,
) !AutoArrayHashMap(u32, c.VkDeviceQueueCreateInfo) {
    var map = AutoArrayHashMap(u32, c.VkDeviceQueueCreateInfo).init(allocator);

    for ([_]u32{ graphics, present }) |index| {
        const gop = try map.getOrPut(index);

        if (!gop.found_existing) {
            gop.value_ptr.* = .{
                .sType = c.VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
                .flags = 0,
                .queueFamilyIndex = @as(u32, @intCast(index)),
                .queueCount = 1,
                .pQueuePriorities = &@as(f32, 1.0),
                .pNext = null,
            };
        }
    }

    return map;
}

pub fn toShaderModule(device: c.VkDevice, code: [:0]const u8) !c.VkShaderModule {
    @setRuntimeSafety(false);

    return try vulkan.acquire(
        c.VkShaderModule,
        c.vkCreateShaderModule,
        .{
            device,
            &c.VkShaderModuleCreateInfo{
                .sType = c.VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
                .codeSize = code.len,
                .pCode = @as(*const u32, @ptrCast(@alignCast(code))),
                .flags = 0,
                .pNext = null,
            },
            null,
        },
    );
}

fn stageToCommandBuffer(
    device: c.VkDevice,
    command_pool: c.VkCommandPool,
    queue: c.VkQueue,
    data: anytype,
    buffer: c.VkBuffer,
    size: c.VkDeviceSize,
    types: []const c.VkMemoryType,
) !void {
    const staging = try Buffer.init(
        device,
        size,
        c.VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        types,
        c.VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | c.VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
    );
    defer staging.deinit(device);

    try mapMemory(device, data, staging.memory, size);

    try copyBuffer(
        device,
        command_pool,
        queue,
        staging.buffer,
        buffer,
        size,
    );
}

fn mapMemory(
    device: c.VkDevice,
    source: anytype,
    destination: c.VkDeviceMemory,
    size: c.VkDeviceSize,
) !void {
    const data = try vulkan.acquire(?*anyopaque, c.vkMapMemory, .{
        device,
        destination,
        0,
        size,
        0,
    });
    _ = c.memcpy(data, @as([*]const u8, @ptrCast(source)), size);
    c.vkUnmapMemory(device, destination);
}

fn copyBuffer(
    device: c.VkDevice,
    command_pool: c.VkCommandPool,
    queue: c.VkQueue,
    source: c.VkBuffer,
    destination: c.VkBuffer,
    size: c.VkDeviceSize,
) !void {
    const command_buffer = try beginCommandBuffer(device, command_pool);

    c.vkCmdCopyBuffer(command_buffer, source, destination, 1, &c.VkBufferCopy{
        .srcOffset = 0,
        .dstOffset = 0,
        .size = size,
    });

    try endCommandBuffer(device, command_buffer, command_pool, queue);
}

fn transitionImageLayout(
    device: c.VkDevice,
    command_pool: c.VkCommandPool,
    queue: c.VkQueue,
    image: c.VkImage,
    old: c.VkImageLayout,
    new: c.VkImageLayout,
    aspect_mask: c.VkImageAspectFlags,
    src_stage_mask: c.VkPipelineStageFlags,
    dst_stage_mask: c.VkPipelineStageFlags,
    src_access_mask: c.VkAccessFlags,
    dst_access_mask: c.VkAccessFlags,
) !void {
    const command_buffer = try beginCommandBuffer(device, command_pool);

    c.vkCmdPipelineBarrier(
        command_buffer,
        src_stage_mask,
        dst_stage_mask,
        0,
        0,
        null,
        0,
        null,
        1,
        &c.VkImageMemoryBarrier{
            .sType = c.VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
            .oldLayout = old,
            .newLayout = new,
            .srcQueueFamilyIndex = c.VK_QUEUE_FAMILY_IGNORED,
            .dstQueueFamilyIndex = c.VK_QUEUE_FAMILY_IGNORED,
            .image = image,
            .subresourceRange = c.VkImageSubresourceRange{
                .aspectMask = aspect_mask,
                .baseMipLevel = 0,
                .levelCount = 1,
                .baseArrayLayer = 0,
                .layerCount = 1,
            },
            .srcAccessMask = src_access_mask,
            .dstAccessMask = dst_access_mask,
            .pNext = null,
        },
    );

    try endCommandBuffer(device, command_buffer, command_pool, queue);
}

fn copyBufferToImage(
    device: c.VkDevice,
    command_pool: c.VkCommandPool,
    queue: c.VkQueue,
    buffer: c.VkBuffer,
    image: c.VkImage,
    width: u32,
    height: u32,
) !void {
    const command_buffer = try beginCommandBuffer(device, command_pool);

    c.vkCmdCopyBufferToImage(
        command_buffer,
        buffer,
        image,
        c.VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        1,
        &c.VkBufferImageCopy{ .bufferOffset = 0, .bufferRowLength = 0, .bufferImageHeight = 0, .imageSubresource = c.VkImageSubresourceLayers{
            .aspectMask = c.VK_IMAGE_ASPECT_COLOR_BIT,
            .mipLevel = 0,
            .baseArrayLayer = 0,
            .layerCount = 1,
        }, .imageOffset = c.VkOffset3D{
            .x = 0,
            .y = 0,
            .z = 0,
        }, .imageExtent = c.VkExtent3D{
            .width = width,
            .height = height,
            .depth = 1,
        } },
    );

    try endCommandBuffer(device, command_buffer, command_pool, queue);
}

fn beginCommandBuffer(
    device: c.VkDevice,
    command_pool: c.VkCommandPool,
) !c.VkCommandBuffer {
    const command_buffer = try vulkan.acquire(
        c.VkCommandBuffer,
        c.vkAllocateCommandBuffers,
        .{
            device,
            &c.VkCommandBufferAllocateInfo{
                .sType = c.VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
                .level = c.VK_COMMAND_BUFFER_LEVEL_PRIMARY,
                .commandPool = command_pool,
                .commandBufferCount = 1,
                .pNext = null,
            },
        },
    );

    try vulkan.err(c.vkBeginCommandBuffer(
        command_buffer,
        &c.VkCommandBufferBeginInfo{
            .sType = c.VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
            .flags = c.VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
            .pInheritanceInfo = null,
            .pNext = null,
        },
    ));

    return command_buffer;
}

fn endCommandBuffer(
    device: c.VkDevice,
    command_buffer: c.VkCommandBuffer,
    command_pool: c.VkCommandPool,
    queue: c.VkQueue,
) !void {
    try vulkan.err(c.vkEndCommandBuffer(command_buffer));

    try vulkan.err(c.vkQueueSubmit(queue, 1, &c.VkSubmitInfo{
        .sType = c.VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .waitSemaphoreCount = 0,
        .pWaitSemaphores = null,
        .pWaitDstStageMask = null,
        .commandBufferCount = 1,
        .pCommandBuffers = &command_buffer,
        .signalSemaphoreCount = 0,
        .pSignalSemaphores = null,
        .pNext = null,
    }, null));
    try vulkan.err(c.vkQueueWaitIdle(queue));

    c.vkFreeCommandBuffers(device, command_pool, 1, &command_buffer);
}

pub fn tick(self: *Vulkan, comptime size: anytype, args: anytype) !bool {
    const frame = self.frames[self.i];
    const buffer = self.buffers[self.i];

    const index = frame.next(self.device, self.swapchain.swapchain) catch |err| switch (err) {
        VulkanError.OutOfDateKhr => {
            try self.reinit(size, args);
            return true;
        },
        else => return err,
    };

    try frame.preflight(self.device);

    try vulkan.err(c.vkResetCommandBuffer(buffer, 0));

    try vulkan.err(c.vkBeginCommandBuffer(
        buffer,
        &c.VkCommandBufferBeginInfo{
            .sType = c.VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
            .flags = 0,
            .pInheritanceInfo = null,
            .pNext = null,
        },
    ));

    const clear_values = [_]c.VkClearValue{
        .{
            .color = .{
                .float32 = [_]f32{
                    0.0,
                    0.0,
                    0.0,
                    0.0,
                },
            },
        },
        .{
            .depthStencil = .{
                .depth = 1.0,
                .stencil = 0,
            },
        },
    };

    c.vkCmdBeginRenderPass(
        buffer,
        &c.VkRenderPassBeginInfo{
            .sType = c.VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
            .renderPass = self.renderpass,
            .framebuffer = self.swapchain.framebuffers[index],
            .renderArea = .{
                .offset = .{
                    .x = 0,
                    .y = 0,
                },
                .extent = self.swapchain.extent,
            },
            .clearValueCount = clear_values.len,
            .pClearValues = &clear_values,
            .pNext = null,
        },
        c.VK_SUBPASS_CONTENTS_INLINE,
    );

    c.vkCmdBindPipeline(buffer, c.VK_PIPELINE_BIND_POINT_GRAPHICS, self.pipeline);

    c.vkCmdSetViewport(buffer, 0, 1, &toViewport(self.swapchain.extent));

    c.vkCmdSetScissor(buffer, 0, 1, &toScissor(self.swapchain.extent));

    const offset: c.VkDeviceSize = 0;

    c.vkCmdBindVertexBuffers(buffer, 0, 1, &self.vertex_buffer.buffer, &offset);

    c.vkCmdBindIndexBuffer(buffer, self.index_buffer.buffer, 0, c.VK_INDEX_TYPE_UINT32);

    c.vkCmdBindDescriptorSets(buffer, c.VK_PIPELINE_BIND_POINT_GRAPHICS, self.layout, 0, 1, &self.descriptor_sets[self.i], 0, null);

    c.vkCmdDrawIndexed(buffer, @intCast(self.indices_len), 1, 0, 0, 0);

    c.vkCmdEndRenderPass(buffer);

    try vulkan.err(c.vkEndCommandBuffer(buffer));

    const uniform_buffer = self.uniform_buffers[self.i];
    const duration = now() - self.start;
    var ubo: UniformBufferObject = undefined;

    c.glm_mat4_identity(&ubo.model);

    c.glmc_rotate(&ubo.model, (@as(f32, @floatFromInt(duration)) / 1000.0) * c.glm_rad(90), f32Unconst(&c.vec3{ 0, 0, 1 }));

    c.glm_lookat(f32Unconst(&c.vec3{ 2, 2, 2 }), f32Unconst(&c.vec3{ 0, 0, 0 }), f32Unconst(&c.vec3{ 0, 0, 1 }), &ubo.view);

    c.glm_perspective(c.glm_rad(45), @as(f32, @floatFromInt(self.swapchain.extent.width)) / @as(f32, @floatFromInt(self.swapchain.extent.height)), 0.1, 10, &ubo.projection);

    ubo.projection[1][1] *= -1;

    _ = c.memcpy(@as([*]u8, @ptrCast(uniform_buffer.mapping)), @as([*]u8, @ptrCast(&ubo)), @sizeOf(@TypeOf(ubo)));

    try frame.submit(self.graphics.queue, buffer);

    const result = frame.present(self.present.queue, self.swapchain.swapchain, index) catch |err| switch (err) {
        VulkanError.OutOfDateKhr => c.VK_ERROR_OUT_OF_DATE_KHR,
        else => return err,
    };

    self.resize = switch (result) {
        c.VK_SUBOPTIMAL_KHR => true,
        c.VK_ERROR_OUT_OF_DATE_KHR => true,
        else => self.resize,
    };

    if (self.resize) {
        self.resize = false;
        try self.reinit(size, args);
    }

    self.i = (self.i + 1) % @as(u32, @intCast(self.frames.len));

    return true;
}

fn reinit(self: *Vulkan, comptime size: anytype, args: anytype) !void {
    var extent: c.VkExtent2D = @call(.auto, size, args);

    while (extent.width == 0 or extent.height == 0) {
        extent = @call(.auto, size, args);
    }

    try self.waitIdle();

    const depth_format = c.VK_FORMAT_D32_SFLOAT; //TODO Compute "best" format.

    const memory_properties = vulkan.get(
        c.VkPhysicalDeviceMemoryProperties,
        c.vkGetPhysicalDeviceMemoryProperties,
        .{self.physical},
    );

    const memory_types = memory_properties.memoryTypes[0..memory_properties.memoryTypeCount];

    try self.swapchain.reinit(
        self.device,
        self.graphics.index,
        self.present.index,
        self.surface,
        self.format,
        extent,
        try vulkan.acquire(
            c.VkSurfaceCapabilitiesKHR,
            c.vkGetPhysicalDeviceSurfaceCapabilitiesKHR,
            .{ self.physical, self.surface },
        ),
        self.mode,
        depth_format,
        memory_types,
        self.renderpass,
    );
}

fn toViewport(extent: c.VkExtent2D) c.VkViewport {
    return .{
        .x = 0,
        .y = 0,
        .width = @as(f32, @floatFromInt(extent.width)),
        .height = @as(f32, @floatFromInt(extent.height)),
        .minDepth = 0,
        .maxDepth = 1,
    };
}

fn toScissor(extent: c.VkExtent2D) c.VkRect2D {
    return .{
        .offset = .{
            .x = 0,
            .y = 0,
        },
        .extent = extent,
    };
}

fn f32Unconst(value: anytype) *f32 {
    return unconst(*f32, value);
}

fn unconst(comptime DestType: type, value: anytype) DestType {
    return @as(DestType, @ptrFromInt(@intFromPtr(value)));
}

pub fn waitIdle(self: *Vulkan) !void {
    try vulkan.err(c.vkDeviceWaitIdle(self.device));
}

pub fn deinit(self: *Vulkan) void {
    const device = self.device;

    defer c.vkDestroyDevice(device, null);

    defer c.vkDestroyShaderModule(device, self.vert, null);

    defer c.vkDestroyShaderModule(device, self.frag, null);

    defer c.vkDestroyDescriptorSetLayout(device, self.descriptor_set_layout, null);

    defer c.vkDestroyPipelineLayout(device, self.layout, null);

    defer c.vkDestroyRenderPass(device, self.renderpass, null);

    defer self.swapchain.deinit(device);

    defer c.vkDestroyPipeline(device, self.pipeline, null);

    defer c.vkDestroyCommandPool(device, self.pool, null);

    defer self.texture.deinit(device);

    defer c.vkDestroyImageView(device, self.texture_view, null);

    defer c.vkDestroySampler(device, self.sampler, null);

    defer self.vertex_buffer.deinit(device);

    defer self.index_buffer.deinit(device);

    defer self.allocator.free(self.uniform_buffers);

    for (self.uniform_buffers) |uniform_buffer| {
        defer uniform_buffer.deinit(device);
    }

    defer c.vkDestroyDescriptorPool(device, self.descriptor_pool, null);

    defer self.allocator.free(self.descriptor_sets);

    for (self.frames) |frame| {
        defer frame.deinit(device);
    }
}
