const Image = @This();

const c = @import("root").c;
const vulkan = @import("../vulkan.zig");

image: c.VkImage,
memory: c.VkDeviceMemory,

pub fn init(
    device: c.VkDevice,
    width: u32,
    height: u32,
    format: c.VkFormat,
    tiling: c.VkImageTiling,
    usage: c.VkImageUsageFlags,
    types: []const c.VkMemoryType,
    properties: c.VkMemoryPropertyFlags,
) !Image {
    const image = try vulkan.acquire(
        c.VkImage,
        c.vkCreateImage,
        .{
            device,
            &c.VkImageCreateInfo{
                .sType = c.VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
                .imageType = c.VK_IMAGE_TYPE_2D,
                .extent = .{
                    .width = width,
                    .height = height,
                    .depth = 1,
                },
                .mipLevels = 1,
                .arrayLayers = 1,
                .format = format,
                .tiling = tiling,
                .initialLayout = c.VK_IMAGE_LAYOUT_UNDEFINED,
                .usage = usage,
                .samples = c.VK_SAMPLE_COUNT_1_BIT,
                .sharingMode = c.VK_SHARING_MODE_EXCLUSIVE,
                .queueFamilyIndexCount = 0,
                .pQueueFamilyIndices = null,
                .flags = 0,
                .pNext = null,
            },
            null,
        },
    );

    const memory_requirements = vulkan.get(
        c.VkMemoryRequirements,
        c.vkGetImageMemoryRequirements,
        .{ device, image },
    );

    const memory = try vulkan.acquire(
        c.VkDeviceMemory,
        c.vkAllocateMemory,
        .{
            device,
            &c.VkMemoryAllocateInfo{
                .sType = c.VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
                .allocationSize = memory_requirements.size,
                .memoryTypeIndex = try vulkan.toMemoryType(
                    types,
                    memory_requirements.memoryTypeBits,
                    properties,
                ),
                .pNext = null,
            },
            null,
        },
    );

    try vulkan.err(c.vkBindImageMemory(device, image, memory, 0));

    return Image{
        .image = image,
        .memory = memory,
    };
}

pub fn deinit(image: *const Image, device: c.VkDevice) void {
    defer c.vkDestroyImage(device, image.image, null);
    defer c.vkFreeMemory(device, image.memory, null);
}
