const UniformBuffer = @This();

const c = @import("root").c;
const vulkan = @import("../vulkan.zig");
const Buffer = @import("Buffer.zig");

buffer: Buffer,
mapping: ?*anyopaque,

pub fn init(
    device: c.VkDevice,
    size: c.VkDeviceSize,
    types: []const c.VkMemoryType,
) !UniformBuffer {
    const buffer = try Buffer.init(
        device,
        size,
        c.VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        types,
        c.VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | c.VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
    );

    const mapping = try vulkan.acquire(?*anyopaque, c.vkMapMemory, .{
        device,
        buffer.memory,
        0,
        size,
        0,
    });

    return .{
        .buffer = buffer,
        .mapping = mapping,
    };
}

pub fn deinit(uniform_buffer: UniformBuffer, device: c.VkDevice) void {
    defer uniform_buffer.buffer.deinit(device);
}
