const ObjectFile = @This();

const std = @import("std");
const fs = std.fs;
const mem = std.mem;
const Allocator = mem.Allocator;

const c = @import("root").c;

const Vertex = @import("Vertex.zig");

attributes: c.tinyobj_attrib_t,
shapes: []c.tinyobj_shape_t,
materials: []c.tinyobj_material_t,

pub fn init(
    comptime buffer_size: usize,
    allocator: Allocator,
    pathname: [:0]const u8,
) !ObjectFile {
    const path = try fs.realpathAlloc(allocator, pathname);
    defer allocator.free(path);

    var attributes: c.tinyobj_attrib_t = undefined;

    var shapes: [buffer_size]c.tinyobj_shape_t = undefined;
    var shapes_len: usize = undefined;

    var materials: [buffer_size]c.tinyobj_material_t = undefined;
    var materials_len: usize = undefined;

    var path_z = try allocator.alloc(u8, path.len + 1);
    defer allocator.free(path_z);

    mem.copyForwards(u8, path_z, path);
    path_z[path.len] = 0;

    const result = c.tinyobj_parse_obj(
        &attributes,
        @ptrCast(shapes[0..].ptr),
        &shapes_len,
        @ptrCast(materials[0..].ptr),
        &materials_len,
        path_z[0..].ptr,
        callback,
        @constCast(@ptrCast(&allocator)),
        0,
    );

    try err(result);

    return ObjectFile{
        .attributes = attributes,
        .shapes = shapes[0..shapes_len],
        .materials = materials[0..materials_len],
    };
}

pub fn vertices(obj: *const ObjectFile, allocator: Allocator) ![]Vertex {
    return try toVertices(&obj.attributes, allocator);
}

fn toVertices(attributes: *const c.tinyobj_attrib_t, allocator: Allocator) ![]Vertex {
    const num_faces = attributes.num_faces;
    const faces = attributes.faces;
    const verts = attributes.vertices;
    const texcoords = attributes.texcoords;

    var result = try allocator.alloc(Vertex, num_faces);

    for (0..num_faces) |i| {
        const face = faces[i];
        const v_idx: usize = @intCast(face.v_idx);
        const vt_idx: usize = @intCast(face.vt_idx);

        result[i] = .{
            .position = .{
                verts[3 * v_idx + 0],
                verts[3 * v_idx + 1],
                verts[3 * v_idx + 2],
            },
            .color = .{ 1.0, 1.0, 1.0 },
            .texture = .{
                texcoords[2 * vt_idx + 0],
                texcoords[2 * vt_idx + 1],
            },
        };
    }

    return result;
}

pub fn indices(obj: *const ObjectFile, allocator: Allocator) ![]u32 {
    const num_faces = obj.attributes.num_faces;
    var result = try allocator.alloc(u32, num_faces);

    for (0..num_faces) |i| {
        result[i] = @intCast(i);
    }

    return result;
}

const TinyobjError = error{
    NoSuccess,
    Empty,
    InvalidParameter,
    FileOperation,
};

fn err(result: c_int) TinyobjError!void {
    switch (result) {
        c.TINYOBJ_ERROR_EMPTY => return TinyobjError.Empty,
        c.TINYOBJ_ERROR_INVALID_PARAMETER => return TinyobjError.InvalidParameter,
        c.TINYOBJ_ERROR_FILE_OPERATION => return TinyobjError.FileOperation,
        else => if (result != c.TINYOBJ_SUCCESS) {
            return TinyobjError.NoSuccess;
        },
    }
}

fn callback(
    ctx: ?*anyopaque,
    filename: ?[*]const u8,
    is_mtl: i32,
    obj_filename: ?[*]const u8,
    buf: ?*?[*]u8,
    len: ?*usize,
) callconv(.C) void {
    _ = filename;
    _ = is_mtl;

    const allocator: *Allocator = @ptrCast(@alignCast(ctx.?));
    const file = fs.openFileAbsoluteZ(@ptrCast(obj_filename.?), .{}) catch unreachable;
    const data = file.readToEndAlloc(allocator.*, @truncate(-1)) catch unreachable;

    //TODO free data

    buf.?.* = data[0..].ptr;
    len.?.* = data.len;
}
