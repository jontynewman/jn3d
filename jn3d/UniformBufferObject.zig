const root = @import("root");
const c = root.c;
const cglm_align_mat = root.cglm_align_mat;

model: c.mat4 align(cglm_align_mat),
view: c.mat4 align(cglm_align_mat),
projection: c.mat4 align(cglm_align_mat),
