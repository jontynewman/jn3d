pub const c = @cImport({
    @cDefine("CGLM_FORCE_DEPTH_ZERO_TO_ONE", {});
    @cDefine("GLFW_INCLUDE_VULKAN", {});
    @cInclude("cglm/cglm.h");
    @cInclude("cglm/call.h");
    @cInclude("GLFW/glfw3.h");
    @cInclude("stb_image.h");
    @cInclude("string.h");
    @cInclude("tinyobj_loader_c.h");
});

const std = @import("std");
const fs = std.fs;
const mem = std.mem;
const Allocator = mem.Allocator;
const process = std.process;

pub const cglm_align_mat = for (@typeInfo(c).Struct.decls) |decl|
{
    @setEvalBranchQuota(100000);
    if (mem.eql(u8, decl.name, "__AVX__")) {
        break 32;
    }
} else 16;

const jn3d = @import("jn3d");
const vulkan = jn3d.vulkan;
const Vulkan = vulkan.Vulkan;

pub fn main() !void {
    const name = "jn3d demo";

    if (c.GLFW_TRUE != c.glfwInit()) {
        return fail();
    }
    defer c.glfwTerminate();

    c.glfwWindowHint(c.GLFW_CLIENT_API, c.GLFW_NO_API);

    const window = c.glfwCreateWindow(800, 600, name, null, null) orelse return fail();
    defer c.glfwDestroyWindow(window);

    const app = toApplicationInfo(name);
    const info = toInstanceInfo(&app);

    const instance = try vulkan.acquire(
        c.VkInstance,
        c.vkCreateInstance,
        .{ &info, null },
    );
    defer c.vkDestroyInstance(instance, null);

    const surface = try vulkan.acquire(
        c.VkSurfaceKHR,
        c.glfwCreateWindowSurface,
        .{ instance, window, null },
    );
    defer c.vkDestroySurfaceKHR(instance, surface, null);

    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    var allocator = gpa.allocator();

    var args = try process.argsWithAllocator(allocator);
    defer args.deinit();

    if (!args.skip()) {
        return error.TooFewArgs;
    }

    const obj_pathname = args.next() orelse return error.TooFewArgs;
    const obj = try jn3d.ObjectFile.init(4096, allocator, obj_pathname);

    const vertices = try obj.vertices(allocator);
    defer allocator.free(vertices);

    const indices = try obj.indices(allocator);
    defer allocator.free(indices);

    var texture_data: ?[]const u8 = null;

    const texture_pathname_arg = args.next();

    if (texture_pathname_arg) |texture_pathname| {
        texture_data = try toTextureData(allocator, texture_pathname);
    }

    if (args.next()) |_| {
        return error.TooManyArgs;
    }

    var demo = try Vulkan.init(
        allocator,
        instance,
        surface,
        toExtent(window),
        2,
        vertices,
        indices,
        texture_data,
    );
    defer demo.deinit();

    if (texture_data) |data| {
        allocator.free(data);
    }

    c.glfwSetWindowUserPointer(window, &demo);
    _ = c.glfwSetFramebufferSizeCallback(window, resize);

    while (tick(window) and try demo.tick(toExtent, .{window})) {}

    try demo.waitIdle();
}

fn resize(window: ?*c.GLFWwindow, _: c_int, _: c_int) callconv(.C) void {
    const ptr = c.glfwGetWindowUserPointer(window);
    const demo: *Vulkan = @ptrCast(@alignCast(ptr));

    demo.resize = true;
}

fn toApplicationInfo(name: [:0]const u8) c.VkApplicationInfo {
    return .{
        .sType = c.VK_STRUCTURE_TYPE_APPLICATION_INFO,
        .pApplicationName = name.ptr,
        .applicationVersion = c.VK_MAKE_VERSION(0, 1, 0),
        .pEngineName = "jn3d",
        .engineVersion = c.VK_MAKE_VERSION(0, 1, 0),
        .apiVersion = c.VK_API_VERSION_1_3,
        .pNext = null,
    };
}

fn toInstanceInfo(info: *const c.VkApplicationInfo) c.VkInstanceCreateInfo {
    var count: u32 = undefined;
    const names = c.glfwGetRequiredInstanceExtensions(&count);

    return .{
        .sType = c.VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
        .pApplicationInfo = info,
        .flags = 0,
        .ppEnabledExtensionNames = @ptrCast(names),
        .enabledExtensionCount = count,
        .ppEnabledLayerNames = @ptrCast(&vulkan.layers),
        .enabledLayerCount = @intCast(vulkan.layers.len),
        .pNext = null,
    };
}

fn toTextureData(allocator: Allocator, pathname: []const u8) ![]u8 {
    const path = try fs.realpathAlloc(allocator, pathname);
    defer allocator.free(path);

    const file = try fs.openFileAbsolute(path, .{});

    return try file.readToEndAlloc(allocator, @truncate(-1));
}

fn toExtent(window: *c.GLFWwindow) c.VkExtent2D {
    var extent: c.VkExtent2D = undefined;

    c.glfwGetFramebufferSize(
        window,
        @ptrCast(&extent.width),
        @ptrCast(&extent.height),
    );

    if (0 == extent.width or 0 == extent.height) {
        c.glfwWaitEvents();
    }

    return extent;
}

fn tick(window: *c.GLFWwindow) bool {
    c.glfwPollEvents();

    return c.GLFW_FALSE == c.glfwWindowShouldClose(window);
}

fn fail() anyerror {
    var description: [*c]const u8 = undefined;
    const code = c.glfwGetError(&description);

    if (c.GLFW_NO_ERROR != code) {
        std.log.err("{s}\n", .{description});
    }

    return error.Glfw;
}
